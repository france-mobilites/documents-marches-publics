package com.xwiki.projects.francemobilites;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Provider;
import javax.inject.Singleton;

import org.slf4j.Logger;
import org.xwiki.batchimport.BatchImportConfiguration;
import org.xwiki.batchimport.RowDataPostprocessor;
import org.xwiki.component.annotation.Component;
import org.xwiki.model.reference.DocumentReferenceResolver;

import com.xpn.xwiki.XWiki;
import com.xpn.xwiki.XWikiContext;
import com.xpn.xwiki.XWikiException;
import com.xpn.xwiki.objects.classes.BaseClass;

/**
 * Allow : 1) Process the value of the 'marche_object' column to have a maximum length of 255 characters in order to be
 * used as a document title for documents of type 'marchés publics'. 2) Set the type property of the
 * Documents.Code.DocumentsClass to the adequate value when importing 'marchés publics' documents.
 * 
 * @version $Id$
 */
@Component("publiccontractprocessor")
@Singleton
public class PublicContractPostProcessor implements RowDataPostprocessor
{

    private static final String PUBLIC_CONTRACT_CLASS = "FMCode.PublicContractClass";

    private static final String DOCUMENT_CLASS = "Documents.Code.DocumentsClass";

    private static final String TITLE_FIELD = "doc.title";

    private static final String TYPE_FIELD = "type";

    private static final String PUBLIC_CONTRACT_TYPE = "marche_public";

    @Inject
    private Provider<XWikiContext> xwikiContextProvider;

    @Inject
    private Logger logger;

    @Inject
    @Named("current")
    private DocumentReferenceResolver<String> resolver;

    /**
     * {@inheritDoc}
     * 
     * @see org.xwiki.batchimport.RowDataPostprocessor#postProcessRow(java.util.Map, java.util.List, int, java.util.Map,
     *      java.util.List, org.xwiki.batchimport.BatchImportConfiguration)
     */
    @Override
    public void postProcessRow(Map<String, String> data, List<String> row, int rowIndex, Map<String, String> mapping,
        List<String> headers, BatchImportConfiguration config)
    {
        try {
            logger.debug("MAPPING : {}", mapping);
            String classReference = config.getMappingClassName();
            XWikiContext xcontext = this.xwikiContextProvider.get();
            XWiki xwiki = xcontext.getWiki();
            BaseClass mappedClass = xwiki.getXClass(resolver.resolve(classReference), xcontext);
            if (mappedClass.equals(xwiki.getXClass(resolver.resolve(PUBLIC_CONTRACT_CLASS), xcontext))) {
                String columnValue = data.get(TITLE_FIELD);
                if (columnValue != null && columnValue.length() > 255) {
                    data.put(TITLE_FIELD, columnValue.substring(0, 255));
                    logger.debug(
                        "Set 'marché public' document title with a truncated value of the 'object_marche' [row : {}]",
                        rowIndex);
                }
            } else if (mappedClass.equals(xwiki.getXClass(resolver.resolve(DOCUMENT_CLASS), xcontext))
                && PUBLIC_CONTRACT_TYPE.equals(mapping.get(TYPE_FIELD))) {
                data.put(TYPE_FIELD, PUBLIC_CONTRACT_TYPE);
                logger.debug(
                    "Set the 'marché public' document type property on Documents.Code.DocumentClass object [row : {}]",
                    rowIndex);
            }
        } catch (XWikiException e) {
            logger.warn("Exception while post processing a 'marché public' for [mapping : {}, config : {}, row : {}]",
                mapping, config, rowIndex, e);
        }
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.xwiki.batchimport.RowDataPostprocessor#getPriority()
     */
    @Override
    public double getPriority()
    {
        return 10;
    }

}
