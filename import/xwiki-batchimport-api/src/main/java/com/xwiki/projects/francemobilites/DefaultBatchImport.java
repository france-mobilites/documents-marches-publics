/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package com.xwiki.projects.francemobilites;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

import org.apache.commons.lang.StringUtils;
import org.xwiki.batchimport.BatchImportConfiguration;
import org.xwiki.batchimport.log.BatchImportLog;
import org.xwiki.component.annotation.Component;
import org.xwiki.model.reference.DocumentReference;
import org.xwiki.officeimporter.document.XDOMOfficeDocument;
import org.xwiki.officeimporter.script.OfficeImporterScriptService;
import org.xwiki.script.service.ScriptService;

import com.xpn.xwiki.XWikiContext;
import com.xpn.xwiki.XWikiException;
import com.xpn.xwiki.api.Document;
import com.xpn.xwiki.doc.XWikiAttachment;
import com.xpn.xwiki.doc.XWikiDocument;

/**
 * Override of the BatchImport API to use InputStream to create files on the file system instead to avoid loading the
 * entire files on memory which can cause the failure of the import.
 *
 * @version $Id$
 */
@Component
public class DefaultBatchImport extends org.xwiki.batchimport.internal.DefaultBatchImport
{
    @SuppressWarnings("deprecation")
    @Override
    public void addFiles(Document newDoc, String path) throws IOException
    {
        File dirFile = new File(path);
        for (File file : dirFile.listFiles()) {
            debug("Adding file " + file.getName());
            InputStream is = new FileInputStream(file.getPath());
            addFile(newDoc, is, file.getName());
        }
    }

    @SuppressWarnings("deprecation")
    public void addFile(Document newDoc, InputStream fileInputStream, String filename)
    {
        try {
            if (filename.startsWith("./")) {
                filename = filename.substring(2);
            }
            XWikiContext xcontext = getXWikiContext();
            filename = xcontext.getWiki().clearName(filename, false, true, xcontext);

            if (newDoc.getAttachment(filename) != null) {
                debug("Filename " + filename + " already exists in " + newDoc.getPrefixedFullName() + ".");
                return;
            }

            // this is saving the document at this point. I don't know if it was like this when the code was written,
            // but now it's like this.
            // WARNING: this will work only if the performer of the import has programming rights.
            XWikiDocument protectedDocument = newDoc.getDocument();
            XWikiAttachment attachment = new XWikiAttachment();
            attachment.setContent(fileInputStream);
            attachment.setFilename(filename);
            attachment.setAuthor(xcontext.getUser());
            // Add the attachment to the document
            attachment.setDoc(protectedDocument);
            protectedDocument.getAttachmentList().add(attachment);
        } catch (Throwable e) {
            debug("Filename " + filename + " could not be attached because of Exception: " + e.getMessage());
        }
    }
    
    @SuppressWarnings("deprecation")
    public InputStream getFileInputStream(ZipFile zipfile, String path) throws ZipException, IOException
    {
        if (zipfile == null) {
            return new FileInputStream(path);
        } else {
            String newpath = path;
            ZipEntry zipentry = zipfile.getEntry(newpath);
            if (zipentry == null) {
                return null;
            }
            return zipfile.getInputStream(zipentry);
        }
    }

    @Override
    public void saveDocumentWithFiles(Document newDoc, Map<String, String> data, List<String> currentLine,
        int rowIndex, BatchImportConfiguration config, XWikiContext xcontext, boolean overwrite, boolean simulation,
        boolean overwritefile, boolean fileimport, boolean fileupload, String datadir, String datadirprefix, ZipFile zipfile,
        boolean filterstyles, List<DocumentReference> savedDocuments, BatchImportLog log) throws XWikiException,
            ZipException, IOException
    {
        String fullName = newDoc.getPrefixedFullName();

        boolean withFile = false;
        boolean fileOk = false;
        String path = "";

        // if there is a mapping for the doc.file field
        String fileMapping = config.getFieldsMapping().get("doc.file");
        if (fileMapping != null) {
            withFile = true;
            path = getFilePath(datadir, datadirprefix, data.get("doc.file"));
            fileOk = checkFile(zipfile, path);
        }

        if (withFile) {
            if (fileOk) {
                debug("Ready to import row " + currentLine.toString() + "in page " + fullName
                    + " and imported file is ok.");

                // if we're simulating we don't actually import the file, we only pretend
                if (simulation) {
                    log.logSave("simimportfileok", rowIndex, currentLine, fullName, path);
                    savedDocuments.add(newDoc.getDocumentReference());
                } else {
                    String fname = getFileName(data.get("doc.file"));
                    // adding the file to the document only if fileupload is true
                    if (fileupload && newDoc.getAttachment(fname) != null) {
                        debug("Filename " + fname + " already exists in " + fullName);

                        // done here
                        // FIXME: this should depend on the overwrite file parameter or at least on the overwrite
                        // one (since overwritefile seems to be about the behavior of the document content when
                        // there are files attached)
                        newDoc.save();
                        savedDocuments.add(newDoc.getDocumentReference());
                        log.logSave("importduplicateattach", rowIndex, currentLine, fullName, path);
                    } else {
                        boolean isDirectory = isDirectory(zipfile, path);
                        if (fileupload && isDirectory) {
                            addFiles(newDoc, path);

                            // done here, we save pointed files in the file and we're done
                            newDoc.save();
                            savedDocuments.add(newDoc.getDocumentReference());
                            log.logSave("importfiledir", rowIndex, currentLine, fullName, path);
                        } else {
                            InputStream fileInputStream = getFileInputStream(zipfile, path);
                            if (fileInputStream != null) {
                                if (fileupload) {
                                    addFile(newDoc, fileInputStream, fname);
                                }
                                // TODO: why the hell are we doing this here?
                                if (overwrite && overwritefile) {
                                    newDoc.setContent("");
                                }

                                // saving the document, in order to be able to do the import properly after
                                newDoc.save();
                                savedDocuments.add(newDoc.getDocumentReference());

                                // launching the openoffice conversion
                                if (fileimport) {
                                    if (!fname.toLowerCase().endsWith(".pdf")
                                        && (StringUtils.isEmpty(newDoc.getContent().trim()) || (overwrite && overwritefile))) {
                                        boolean importResult = false;

                                        try {
                                            OfficeImporterScriptService officeImporter = this.cm.getInstance(ScriptService.class, "officeimporter");
                                            // import the attachment in the content of the document
                                            XDOMOfficeDocument xdomOfficeDoc =
                                                officeImporter.officeToXDOM(fileInputStream, fname, fullName, filterstyles);
                                            importResult =
                                                officeImporter.save(xdomOfficeDoc, fullName, newDoc.getSyntax()
                                                    .toIdString(), null, null, true);
                                        } catch (Exception e) {
                                            LOGGER.warn("Failed to import content from office file " + fname
                                                + " to document " + fullName, e);
                                        }

                                        if (!importResult) {
                                            log.logSave("importofficefail", rowIndex, currentLine, fullName, path);
                                        } else {
                                            log.logSave("importoffice", rowIndex, currentLine, fullName, path);
                                        }

                                        // in case import was unsuccessful let's empty the content again
                                        // to be able to detect it
                                        Document newDoc2 =
                                            xcontext.getWiki().getDocument(newDoc.getDocumentReference(), xcontext)
                                                .newDocument(xcontext);
                                        if (StringUtils.isEmpty(newDoc2.getContent().trim())) {
                                            newDoc2.setContent("");
                                            newDoc2.save();
                                        }
                                        // clean up open office temporary files
                                        cleanUp();
                                    }
                                } else {
                                    log.logSave("importnooffice", rowIndex, currentLine, fullName, path);
                                }
                            } else {
                                log.logSave("importcannotreadfile", rowIndex, currentLine, fullName, path);
                            }
                        }
                    }
                }
            } else {
                log.logError("errornofile", rowIndex, currentLine, fullName, path);
                // TODO: this will leave the document unsaved because of the inexistent file, which impacts the data set
                // with the marshalDataToDocumentObjects function (because of the way doImport is written), so maybe
                // this should be configured by an error handling setting (skip row or skip value, for example), the
                // same as we should have for document data
            }
        } else {
            debug("Ready to import row " + currentLine.toString() + " in page " + fullName + " (no file attached).");

            // we should save the data
            if (simulation == false) {
                newDoc.save();
                log.logSave("importnofile", rowIndex, currentLine, fullName);
            } else {
                log.logSave("simimportnofile", rowIndex, currentLine, fullName);
            }
            savedDocuments.add(newDoc.getDocumentReference());
        }
    }
}
