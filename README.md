# documents marchés publics

l'outil de capitalisation des documents de marchés publics de France Mobilités

# Installation Instructions

## Infrastructure Requirements

**JVM allocated memory**

`MEM_START="256m"
MEM_MAX="1024m"`

## Setup of the wiki

### WAR

This project is installed on top of the standard 10.11.8 XWiki version war (http://maven.xwiki.org/releases/org/xwiki/platform/xwiki-platform-distribution-war/10.11.8/xwiki-platform-distribution-war-10.11.8.war) and default distribution.

When asked by the default distribution to create a user for the wiki, create the user "Admin". Keep this user owner of the main wiki and don't delete it.

The next installation steps should be done with this Admin user.

#### War setup

Standard install:

* Configure the xwiki work directory
* Configure the database access to mysql, hibernate.cfg.xml should be fine, but add the connector in WEB-INF/lib
* Office server setup
* file system attachments storage

Find more details about the xwiki installation [here](https://www.xwiki.org/xwiki/bin/view/Documentation/AdminGuide/Installation/).

### EM extensions installed

* Install [Batch Import Application](https://extensions.xwiki.org/xwiki/bin/view/Extension/Batch%20Import%20Application) 2.2.

## Installation of the custom code

* import the xar xwiki-france-mobilites-wikis-main-<version>.xar obtained from the build of module **wikis/main** in the wiki
* import the xar xwiki-france-mobilites-import-wiki-<version>.xar obtained from the build of module **import/wiki** in the wiki
* Install the jar obtained from the build of module import/postprocessor in WEB-INF/lib
* Install the jar obtained from the build of module import/xwiki-batchimport-api in xwiki WEB-INF/lib
* Install Batch Import API in xwiki WEB-INF/lib and all its dependencies
    * Copy the following jars from the xwiki parmanent directory to WEB-INF/lib
        * ${permadir}/extension/repository/org%2Exwiki%2Econtrib%3Axwiki-batchimport-api/2%2E2/org%2Exwiki%2Econtrib%3Axwiki-batchimport-api-2%2E2.jar
        * ${permadir}/extension/repository/org%2Exwiki%2Econtrib%3Axwiki-plugin-excel/1%2E1/org%2Exwiki%2Econtrib%3Axwiki-plugin-excel-1%2E1.jar
        * ${permadir}/extension/repository/net%2Esourceforge%2Ejexcelapi%3Ajxl/2%2E6%2E12/net%2Esourceforge%2Ejexcelapi%3Ajxl-2%2E6%2E12.jar
* Copy the opencsv-4.5.jar (http://central.maven.org/maven2/com/opencsv/opencsv/4.5/opencsv-4.5.jar) to WEB-INF/lib
* Restart tomcat